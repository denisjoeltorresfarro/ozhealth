<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type"
        content="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MATRIZ</title>
</head>
<link href="{{ mix('css/style_pdf_print.css') }}" rel="stylesheet">
@php
$records = $resources->records;
$keys = array_keys((array)$records[0]);
@endphp
<table>
    <thead>
        <tr>
            @foreach ($keys as $item)
                <th style="font-weight: bold;">{{ $item }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($records as $record)
            <tr>
                @foreach ($keys as $key)
                    <td>{{ $record->$key == '0' ?  '00' : $record->$key}}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
