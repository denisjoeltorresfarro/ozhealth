<!doctype html>
<!--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">-->
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
{{--    <meta name="viewport" content="user-scalable=no" />--}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    @if (config("app.env") === 'local')
    <link rel="shortcut icon" href="/img/pig.png" />
    @else
    <link rel="shortcut icon" href="/img/cruz_roja.png" />
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('/js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ mix('css/app.css')  }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        @php
            $user = Auth::user();
        @endphp
        <app-component
        :env="{{json_encode(config("app.env"))}}"
        :apirust="{{json_encode(env('MIX_URI_RUST'))}}"
        />
    </div>
</body>

</html>
