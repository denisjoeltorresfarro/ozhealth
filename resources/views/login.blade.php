<!doctype html>
<!--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">-->
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <meta name="viewport" content="user-scalable=no" /> --}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    @if (config('app.env') === 'local')
        <link rel="shortcut icon" href="/img/pig.png" />
    @else
        <link rel="shortcut icon" href="/img/hosp-svg.svg" />
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

</head>

<body>
    <!-- partial:index.partial.html -->
    <div class="containerhead">
        <div class="containerheader">CM SINAI | CSO PREVENTIVA</div>
    </div>
    <div class="container" id="container">
        <div class="form-container sign-up-container">
            <form action="#">
                <h1>Resultados Privados</h1>
                <br />
                <br />
                <!-- <div class="social-container">
                    <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <span>ingrese sus datos de acceso</span> -->
                <input type="email" placeholder="DNI" />
                <input type="password" placeholder="Contraseña" />
                <br />
                <br />
                <button>Ingresar</button>
            </form>
        </div>
        <div class="form-container sign-in-container">
            <form id="formato" action="{{ route('login') }}" role="form" method="POST">
                @csrf
                <h1>Resultados Corporativos</h1>
                <br />
                <br />
                <!-- <div class="social-container">
                    <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                </div> -->
                <!-- <span>Ingrese los datos de su cuenta</span> -->
                <input type="text" id="login_user" name="login_user" autocomplete="off" inputmode="text"
                    placeholder="RUC">
                <input type="password" id="pw" name="login_pw" autocomplete="off" inputmode="text"
                    placeholder="Contraseña">
                <input id="FCH_DES" type="date" style="display:none" class="form-control" name="FCH_DES" value=""
                    min="2018-01-01" max="2100-12-31">
                <input id="FCH_HAS" type="date" style="display:none" class="form-control" name="FCH_HAS" value=""
                    min="2018-01-01" max="2100-12-31">
                <br />
                <br />
                <!--<a href="#">Forgot your password?</a>-->
                <button type="submit" form="formato" value="Ingresar">>Ingresar</button>
            </form>
        </div>
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-left">
                    <h1>Hola, Bienvenido!</h1>
                    <p>Si tus examenes son de personas juridicas ingresa aqui</p>
                    <button class="ghost" id="signIn">Ingresa</button>
                </div>
                <div class="overlay-panel overlay-right">
                    <h1>Hola, Bienvenido!</h1>
                    <p>Si tu examen es para personas naturales ingresa aqui</p>
                    <button class="ghost" id="signUp">Ingresa</button>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <p>
            Creado con <i class="fa fa-heart"></i> por
            <a target="_blank" href="https://florin-pop.com">OzHealth Peru</a>
            - Si necesita contactar al proveedor haz clic
            <a target="_blank" href="https://www.ozhealthperu.com/">aqui</a>.
        </p>
    </footer>
    <!-- partial -->
    <script src="./script.js"></script>
    <script>
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0, 10);
        });
        document.getElementById('FCH_DES').value = new Date().toDateInputValue();
        document.getElementById('FCH_HAS').value = new Date().toDateInputValue();

        const signUpButton = document.getElementById('signUp');
        const signInButton = document.getElementById('signIn');
        const container = document.getElementById('container');

        signUpButton.addEventListener('click', () => {
            container.classList.add("right-panel-active");
        });

        signInButton.addEventListener('click', () => {
            container.classList.remove("right-panel-active");
        });
    </script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

        * {
            box-sizing: border-box;
        }

        body {
            background: #f6f5f7;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            font-family: 'Montserrat', sans-serif;
            height: 100vh;
            margin: -20px 0 50px;
        }

        h1 {
            font-weight: bold;
            margin: 0;
        }

        h2 {
            text-align: center;
        }

        p {
            font-size: 14px;
            font-weight: 100;
            line-height: 20px;
            letter-spacing: 0.5px;
            margin: 20px 0 30px;
        }

        span {
            font-size: 12px;
        }

        a {
            color: #333;
            font-size: 14px;
            text-decoration: none;
            margin: 15px 0;
        }

        button {
            border-radius: 20px;
            border: 1px solid #259fdf;
            background-color: #259fdf;
            color: #FFFFFF;
            font-size: 12px;
            font-weight: bold;
            padding: 12px 45px;
            letter-spacing: 1px;
            text-transform: uppercase;
            transition: transform 80ms ease-in;
        }

        button:active {
            transform: scale(0.95);
        }

        button:focus {
            outline: none;
        }

        button.ghost {
            background-color: transparent;
            border-color: #FFFFFF;
        }

        form {
            background-color: #FFFFFF;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 50px;
            height: 100%;
            text-align: center;
        }

        input {
            background-color: #eee;
            border: none;
            padding: 12px 15px;
            margin: 8px 0;
            width: 100%;
        }

        .container {
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25),
                0 10px 10px rgba(0, 0, 0, 0.22);
            position: relative;
            overflow: hidden;
            width: 768px;
            max-width: 100%;
            min-height: 480px;
        }

        .containerhead {
            position: fixed;
            top: 30px;
            width: 100%;
            z-index: 1;
            top: 0px;
        }

        .containerheader {
            padding: 15px;
            /*text-align: center;*/
            background: #222;
            color: white;
            font-size: 30px
        }

        .form-container {
            position: absolute;
            top: 0;
            height: 100%;
            transition: all 0.6s ease-in-out;
        }

        .sign-in-container {
            left: 0;
            width: 50%;
            z-index: 2;
        }

        .container.right-panel-active .sign-in-container {
            transform: translateX(100%);
        }

        .sign-up-container {
            left: 0;
            width: 50%;
            opacity: 0;
            z-index: 1;
        }

        .container.right-panel-active .sign-up-container {
            transform: translateX(100%);
            opacity: 1;
            z-index: 5;
            animation: show 0.6s;
        }

        @keyframes show {

            0%,
            49.99% {
                opacity: 0;
                z-index: 1;
            }

            50%,
            100% {
                opacity: 1;
                z-index: 5;
            }
        }

        .overlay-container {
            position: absolute;
            top: 0;
            left: 50%;
            width: 50%;
            height: 100%;
            overflow: hidden;
            transition: transform 0.6s ease-in-out;
            z-index: 100;
        }

        .container.right-panel-active .overlay-container {
            transform: translateX(-100%);
        }

        .overlay {
            background: #0081ab;
            background: -webkit-linear-gradient(to right, #259fdf, #0081ab);
            background: linear-gradient(to right, #259fdf, #0081ab);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 0 0;
            color: #FFFFFF;
            position: relative;
            left: -100%;
            height: 100%;
            width: 200%;
            transform: translateX(0);
            transition: transform 0.6s ease-in-out;
        }

        .container.right-panel-active .overlay {
            transform: translateX(50%);
        }

        .overlay-panel {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 40px;
            text-align: center;
            top: 0;
            height: 100%;
            width: 50%;
            transform: translateX(0);
            transition: transform 0.6s ease-in-out;
        }

        .overlay-left {
            transform: translateX(-20%);
        }

        .container.right-panel-active .overlay-left {
            transform: translateX(0);
        }

        .overlay-right {
            right: 0;
            transform: translateX(0);
        }

        .container.right-panel-active .overlay-right {
            transform: translateX(20%);
        }

        .social-container {
            margin: 20px 0;
        }

        .social-container a {
            border: 1px solid #DDDDDD;
            border-radius: 50%;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            margin: 0 5px;
            height: 40px;
            width: 40px;
        }

        footer {
            background-color: #222;
            color: #fff;
            font-size: 14px;
            bottom: 0;
            position: fixed;
            left: 0;
            right: 0;
            text-align: center;
            z-index: 999;
        }

        footer p {
            margin: 10px 0;
        }

        footer i {
            color: red;
        }

        footer a {
            color: #3c97bf;
            text-decoration: none;
        }

    </style>

</body>

</html>
