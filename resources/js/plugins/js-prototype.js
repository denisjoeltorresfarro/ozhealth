/**
 * Define the sum method in the prototype of an array
 * that returns a sum value by parameter from all items.
 *
 * @param paramName {String} parameter by which I want to sum
 */
Object.defineProperty(Array.prototype, 'sum', {
    value: function (paramName) {
        var that = this;
        return that.reduce((a, b) => a + parseFloat(b[paramName]), 0);
    }
});
/**
 * Define the sum method in the prototype of an array
 * that returns a sum value by parameter from all items.
 *
 * @param paramName {String} parameter by which I want to sum
 */
Object.defineProperty(Array.prototype, 'groupBy', {
    value: function (criteria) {
        var that = this;
        const newObj = that.reduce(function (acc, currentValue) {
            if (!acc[currentValue[criteria]]) {
                acc[currentValue[criteria]] = [];
            }
            acc[currentValue[criteria]].push(currentValue);
            return acc;
        }, []);
        return newObj.reverse();
    }
});
/**
 * Define the chunk method in the prototype of an array
 * that returns an array with arrays of the given size.
 *
 * @param chunkSize {Integer} Size of every group
 */
Object.defineProperty(Array.prototype, 'chunk', {
    value: function (chunkSize) {
        var that = this;/*
        var results = [];
        while (that.length) {
            results.push(that.splice(0, chunkSize));
        }

        return results;*/
        return Array(Math.ceil(that.length / chunkSize)).fill().map(function (_, i) {
            return that.slice(i * chunkSize, i * chunkSize + chunkSize);
        });
    }
});
/**
 * Define the divide method in the prototype of an array
 * that returns an array with arrays of two colums.
 *
 */
Object.defineProperty(Array.prototype, 'divide', {
    value: function () {
        var that = this;
        var total = [];
        var len = that.length;
        if (len > 1) {
            var table_one = that.slice(0, len / 2 + 0.5);
            total[0] = table_one;
            var table_two = that.slice(len / 2 + 0.5, len);
            total[1] = table_two;
        } else {
            var table_one = that.slice(0, len / 2 + 0.5);
            total[0] = table_one;
        }
        return total;
    }
});
Object.defineProperty(Array.prototype, 'divides', {
    value: function (columns = 2, balanced = true) {
        var that = this;
        if (columns < 2)
            return [that];

        var len = that.length,
            out = [],
            i = 0,
            size;
        if (len % columns === 0) {
            size = Math.floor(len / columns);
            while (i < len) {
                out.push(that.slice(i, i += size));
            }
        } else if (balanced) {
            while (i < len) {
                size = Math.ceil((len - i) / columns--);
                out.push(that.slice(i, i += size));
            }
        } else {
            columns--;
            size = Math.floor(len / columns);
            if (len % size === 0)
                size--;
            while (i < size * columns) {
                out.push(that.slice(i, i += size));
            }
            out.push(that.slice(size * columns));

        }

        return out;
    }
});

Object.defineProperty(Object.prototype, 'inner_assign', {
    value: function (needle) {
        var that = this;
        Object.keys(that).map(function (key, index) {
            if (key in needle) {
                that[key] = needle[key];
            }
        });
        return that;
    }
});
