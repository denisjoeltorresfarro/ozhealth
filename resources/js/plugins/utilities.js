export default {
    install(Vue, options) {
        Vue.prototype.isNull = function (value) {
            return value == null;
        }
    }
}
