import Vue from "vue";
import VueRouter from "vue-router";
import store from '../store/index';

Vue.use(VueRouter);

const files = require.context('./modules/', true, /\.js$/i)
let homeChildrens = new Array();
files.keys().forEach(function (e) { homeChildrens.push(...files(e).default) });
const routes = [
    {
        path: "/login",
        name: "login",
        component: () =>
            import("../views/auth/Login.vue"),
        meta: {
            requiresAuth: false,
        }
    },
    {
        path: "/",
        name: "home",
        redirect: "/atencion",
        component: () =>
            import("../layouts/Dashboard.vue"),
        children: homeChildrens,
        meta: {
            requiresAuth: true,
        }
    }
];


const router = new VueRouter({
    mode: "history",
    hash: false,
    //base: process.env.APP_URL,
    routes
});


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        store.dispatch('authenticate').then((res) => {
            if (res) {
                next();
            } else {
                next({ name: 'login' })
            }
        });
    } else {
        if (to.name == 'login') {
            store.dispatch('authenticate').then((res) => {
                if (res) {
                    router.push({ name: "atencion.index" });
                } else {
                    next();
                }
            });
        } else {
            next();
        }
    }
});

export default router;
