require('./bootstrap');

window.Vue = require('vue').default;

Vue.component('app-component', require('./components/App.vue').default);

//Plugins
import vuetify from './plugins/vuetify';
import router from './routes';
import './plugins';
//support ordenes prototype
import Utilities from './plugins/utilities'
Vue.use(Utilities)
//support js prototype
import JSPrototype from './plugins/js-prototype'
Vue.use(JSPrototype)
//INFINITY LOADING
import InfiniteLoading from 'vue-infinite-loading';
Vue.use(InfiniteLoading, {
    slots: {
        noMore: 'NO EXISTEN MÁS REGISTROS', // you can pass a string value
    },
});
//Store
import store from "./store";

window.base_uri_rust = process.env.MIX_URI_RUST;
store.commit('SET_ENV_VUE', process.env.NODE_ENV);


const app = new Vue({
    el: '#app',
    vuetify,
    store,
    router
});
