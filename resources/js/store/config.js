export const config = {
    state:{
        VueEnv: 'development',
        LaravelEnv: 'local',
        apirust: null
    },
    getters:{
        GET_ENV_VUE: (state)=>{
            return state.VueEnv;
        },
        GET_ENV_LARAVEL: (state)=>{
            return state.LaravelEnv;
        },
        GET_API_RUST: (state)=>{
            return state.apirust;
        },
    },
    actions:{},
    mutations:{
        SET_ENV_VUE(state, value) {
            state.VueEnv = value;
        },
        SET_ENV_LARAVEL(state, value) {
            state.LaravelEnv = value;
        },
        SET_API_RUST(state, value) {
            state.apirust = value;
        },
    }
}
