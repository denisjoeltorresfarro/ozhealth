import store from './index'
import router from '../routes'
export const auth = {
    state: {
        user: null,
        token: null,
        auth: false,
    },
    getters: {
        GET_USER: (state) => {
            return JSON.parse(state.user);
        },
        GET_TOKEN: (state) => {
            if (!state.user) {
               // router.push({ name: 'login' });
            }
            return state.token;
        },
        GET_USER_ID: (state) => {
            let user = JSON.parse(state.user);
            return user.id;
        },
    },
    actions: {
        async loadQueryParams({ commit, dispatch }, newQueryParams) {
            let query = Object.assign({}, router.history.current.query);
            let tempKey = {};
            Object.keys(newQueryParams).forEach((key, index) => {
                if (key in query) {
                    newQueryParams[key] = /^\d+$/.test(query[key])
                        ? Number(query[key])
                        : query[key];

                } else {
                    tempKey[key] = newQueryParams[key];
                    Object.assign(query, tempKey);
                    router.push({ query });
                }
            });
            return newQueryParams;
        },
        asignQueryParams({ commit, dispatch }, newQuery) {
            let query = Object.assign({}, router.history.current.query);
            let tempKey = {};
            Object.keys(newQuery).forEach((key, index) => {
                if (key in query) {
                    let tempValue = /^\d+$/.test(query[key])
                        ? Number(query[key])
                        : query[key];
                    if (newQuery[key] != tempValue) {
                        tempKey[key] = newQuery[key];
                        Object.assign(query, tempKey);
                        router.push({ query });
                    }
                } else {
                    tempKey[key] = newQuery[key];
                    let queryRoot = Object.assign({}, router.history.current.query);
                    Object.assign(queryRoot, tempKey);
                    router.push({ queryRoot });
                }
            });
        },
        async authenticate({ commit, state, dispatch }) {
            /**
             * Si no existe el token, hara un return y acaba el proceso
             */
            if (!state.user) {
                commit('SET_AUTH', false);
                commit('SET_USER', null);
                commit('SET_TOKEN', null);
            }

            return state.auth;
        },
        logout({ commit, state, getters }) {
            commit('SET_AUTH', false);
            commit('SET_USER', null);
            commit('SET_TOKEN', null);
            router.push({ name: 'login' });
        },
    },
    mutations: {
        SET_AUTH(state, value) {
            state.auth = value;
        },
        SET_USER(state, value) {
            if (value) {
                let temp = { id: null, name: null, token: null }
                state.token = value.token;
                state.auth = true;
                state.user = JSON.stringify(Object.assign(temp, value));
            }else{
                state.user = null;
            }
        },
        SET_TOKEN(state, value) {
            state.token = value;
        },
    }
}
