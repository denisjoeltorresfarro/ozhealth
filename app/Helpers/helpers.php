<?php

use App\Helpers\Needle;
use App\Helpers\PeriodoHelper;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

if (!function_exists('needle')) {
    function needle($resource)
    {
        return new Needle($resource);
    }
}

if (!function_exists('removeNullFromArray')) {
    function removeNullFromArray(array $array): array
    {
        return array_filter($array, function ($value) {
            return !is_null($value);
        });
    }
}

if (!function_exists('leftJoinKeysInArray')) {
    function leftJoinKeysInArray(array $needle, array $haystack): array
    {
        return array_filter($needle, function ($key) use ($haystack) {
            return !in_array($key, $haystack);
        }, ARRAY_FILTER_USE_KEY);
    }
}

if (!function_exists('paginate')) {
    function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(array_values($items->forPage($page, $perPage)->toArray()), $items->count(), $perPage, $page, $options);
    }
}

if (!function_exists('perdiodohelper')) {
    function perdiodohelper(array $resources)
    {
        return new PeriodoHelper($resources);
    }
}

