<?php

namespace App\Helpers;

use App\Helpers\Needle;
use Carbon\Carbon;

class PeriodoHelper extends Needle
{

    /**
     * Create a new Periodo instance.
     *
     * @param  array  $resource
     * @return array
     */
    public function __construct(array $resource)
    {
        parent::__construct($this->setData($resource));
    }

    /**
     * Create a new resource instance.
     *
     * @param  array  $resource
     * @return array
     */

    private function setData(array $resource): array
    {
        return [
            'period'        => $this->required($resource, 'period'),
            'month_start'   => in_array($resource['period'], ['month', 'between_months']) ? $this->required($resource, 'month_start') : null,
            'month_end'     => in_array($resource['period'], ['month', 'between_months']) ? $this->required($resource, 'month_end') : null,
            'date_start'   => in_array($resource['period'], ['date', 'between_dates']) ? $this->required($resource, 'date_start') : null,
            'date_end'   => in_array($resource['period'], ['date', 'between_dates']) ? $this->required($resource, 'date_end') : null,
            'time_start' => needle($resource)->time_start,
            'time_end' => needle($resource)->time_end,
        ];
    }

    public function getPeriod($format = 'd-m-Y')
    {
        $d_start     = null;
        $d_end       = null;
        $time_s = $this->time_start ? " $this->time_start" : '';
        $time_e = $this->time_end ? " $this->time_end" : '';
        switch ($this->period) {
            case 'month':
                $d_start = Carbon::parse($this->month_start . '-01')->format($format);
                $d_end   = Carbon::parse($this->month_start . '-01')->endOfMonth()->format($format);
                break;
            case 'between_months':
                $d_start = Carbon::parse($this->month_start . '-01')->format($format);
                $d_end   = Carbon::parse($this->month_end . '-01')->endOfMonth()->format($format);
                break;
            case 'date':
                $d_start = Carbon::parse($this->date_start . $time_s)->format($format);
                $d_end   = Carbon::parse($this->date_end . $time_e)->format($format);
                break;
            case 'between_dates':
                $d_start = Carbon::parse($this->date_start . $time_s)->format($format);
                $d_end   = Carbon::parse($this->date_end . $time_e)->format($format);
                break;
        }
        return (object)['start' => $d_start, 'end' => $d_end];
    }
}
