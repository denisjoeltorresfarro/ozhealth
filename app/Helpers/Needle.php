<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class Needle
{
    protected $resource;

    /**
     * Construct Needle Class.
     *
     * @param  \Illuminate\Database\Eloquent\Model|object|array  $resource
     * @return Needle
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($property)
    {
        return isset($this->toObject()->$property)
            ? $this->toObject()->$property
            : null;
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        if (isset($this->resource)) $this->resource[$key] = $value;
    }

    public function __unset($name)
    {
        if (isset($this->resource)) unset($this->resource[$name]);
    }

    protected function required($haystack, $needle)
    {
        if (isset($haystack[$needle])) {
            return $haystack[$needle];
        } else {
            throw new \Exception("$needle ES REQUERIDO", 1);
        }
    }

    public function getData()
    {
        return $this->resource;
    }

    public function toArray()
    {
        return isset($this->resource) ? $this->resource : [];
    }

    public function toObject()
    {
        return isset($this->resource) ?
            (is_object($this->resource) ?
                $this->resource : ($this->resource instanceof Model ?
                    $this->resource : (is_array($this->resource) ?
                        (object) $this->resource : (object)[]))) : (object)[];
    }
}
