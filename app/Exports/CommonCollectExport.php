<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CommonCollectExport implements FromCollection, WithHeadings
{
    use Exportable;
    public function resources($records, $headers = [])
    {
        $this->records = $records;
        $this->headers = $headers;
        return $this;
    }

    public function collection()
    {
        return $this->records;
    }

    public function headings(): array
    {
        return $this->headers;
    }
}
