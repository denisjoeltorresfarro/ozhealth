<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class CommonExport implements FromView
{
    use Exportable;
    public function resources($resources, $view)
    {
        $this->resources = $resources;
        $this->view = $view;
        return $this;
    }
    /**
     * @return \Illuminate\Contracts\View
     */
    public function view(): View
    {
        return view($this->view, [
            'resources' => $this->resources,
        ]);
    }
}
