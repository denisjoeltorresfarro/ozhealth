<?php

namespace App\Http\Controllers;

use App\Traits\ConsumeExternalServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Jurosh\PDFMerge\PDFMerger;
use Illuminate\Support\Str;
use App\Classes\Zip\Zip;
use App\Classes\Zip\ZipManager;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    use ConsumeExternalServices;

    public function legajo(Request $request)
    {
        set_time_limit(0);
        $ordenes_ids = $request->PARM_ORDENES;
        $uuid = Str::uuid()->toString();

        $res = $this->makeRequest('GET', 'LEGAJO', ['PARM_ORDENES' => $ordenes_ids]);
        if (count($res) > 0 && needle($res[0])->status == 200) {
            $records = $res[0]->records;
            $urls = json_encode($this->structParams($records));
            $zipped = $this->makeRequestStream('GET', 'api/files/zipped', ['urls' => $urls]);

            return Response::make($zipped, 200, [
                'Content-Type' => 'application/zip',
                'Content-Disposition' => 'download; filename="legajo.zip"'
            ]);
        } else {
            return response()->json($res, needle($res[0])->status ?? 500);
        }
    }
    public function legajoDeprecated(Request $request)
    {
        $ordenes_ids = $request->PARM_ORDENES;
        $uuid = Str::uuid()->toString();

        $res = $this->makeRequest('GET', 'LEGAJO', ['PARM_ORDENES' => $ordenes_ids]);
        if (count($res) > 0 && needle($res[0])->status == 200) {
            $records = $res[0]->records;
            foreach ($records as $record) {
                $urls = json_encode($this->structParams($records));
                $res = $this->makeRequestStream('GET', 'api/files/merge', ['urls' => $urls]);
                $filename = str_replace('/', '_', $record->legajo);
                Storage::disk('local')->put("$uuid/$filename.pdf", $res);
            }
        } else {
            return response()->json($res, needle($res[0])->status ?? 500);
        }

        $zip_name = time() . ".zip"; // Zip name
        $zip = Zip::create($zip_name);
        $path = storage_path("app/$uuid");
        $files = File::allFiles($path);
        foreach ($files as $key => $file) {
            $zip->add($file, true);
        }
        //$zip->add($path, true);
        $zip->close();
        Storage::disk('local')->deleteDirectory($uuid);
        return response()->download(public_path($zip_name))->deleteFileAfterSend();
    }
    public function download(Request $request)
    {
        $partial_url = $request->url;
        $full_url = env('BASE_URI_AWS') . "$partial_url";
        $filename = "temporal";
        $tempFile = tempnam(sys_get_temp_dir(), $filename);
        copy($full_url, $tempFile);
        return Response::make(file_get_contents($tempFile), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
    public function downloadMerge(Request $request)
    {
        $partial_urls = json_decode($request->urls);
        $pdf = new PDFMerger;
        foreach ($partial_urls as $key => $partial_url) {
            $full_url = env('BASE_URI_AWS') . "$partial_url";
            $filename = "temporal.pdf";
            $tempFile = tempnam(sys_get_temp_dir(), $filename);
            try {
                copy($full_url, $tempFile);
                $pdf->addPDF($tempFile, 'all');
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        // call merge, output format `file`
        $filename = "temporal-out.pdf";
        $tempFile = tempnam(sys_get_temp_dir(), $filename);
        return $pdf->merge();
    }

    private function structParams($record): array
    {
        $list = [];
        foreach ($record as $value) {
            if (needle($value)->legajo && needle($value)->legajo != "/") {
                array_push($list, $value->legajo);
            }
            if (needle($value)->urls) {
                foreach ($value->urls as $file){
                    array_push($list, $file);
                }
               // array_merge($list, $value->urls);
            }
        }
        return $list;
    }
}
