<?php

namespace App\Http\Controllers;

use App\Traits\ConsumeExternalServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AtencionController extends Controller
{
    use ConsumeExternalServices;

    public function filters()
    {
        $res = $this->makeRequest('GET', 'ATENCIONES_FILTROS');
        $tipos_examen = [];

        if (count($res) > 0) {
            if ($res[0]->status == 200) {
                $tipos_examen = $res[0]->tipos_examen;
            }
        }

        return compact('tipos_examen');
    }
    public function records(Request $request)
    {
        $d_filters = $this->formatDateFilters($request);
        $queryParams = [
            'PARM_FECHA_INI' => $d_filters[0],
            'PARM_FECHA_FIN' => $d_filters[1],
            'PARM_TOKEN' => $request->token,
            'PARM_IDUSER' => $request->user_id,
            'PARM_PACIENTE' => $request->nombre,
            'PARM_TIPO_EXAMEN_ID' => $request->tipo_examen_id,
        ];

        $res = $this->makeRequest('GET', 'ATENCIONES_REGISTROS', $queryParams);

        $res = $res ? (count($res) > 0 ? $res[0] : $res) : $res;

        return response()->json($res, needle($res)->status ?? 500);
    }
    public function formatDateFilters($request)
    {
        $period      = $request['period'];
        $month_start = $request['month_start'];
        $month_end   = $request['month_end'];
        $date_start  = $request['date_start'];
        $date_end    = $request['date_end'];
        $d_start     = null;
        $d_end       = null;

        switch ($period) {
            case 'month':
                $d_start = Carbon::parse($month_start . '-01')->format('Y-m-d');
                $d_end   = Carbon::parse($month_start . '-01')->endOfMonth()->format('Y-m-d');
                break;
            case 'between_months':
                $d_start = Carbon::parse($month_start . '-01')->format('Y-m-d');
                $d_end   = Carbon::parse($month_end . '-01')->endOfMonth()->format('Y-m-d');
                break;
            case 'date':
                $d_start = $date_start;
                $d_end   = $date_start;
                break;
            case 'between_dates':
                $d_start = $date_start;
                $d_end   = $date_end;
                break;
        }

        return [$d_start, $d_end];
    }
}
