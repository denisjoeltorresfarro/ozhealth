<?php

namespace App\Http\Controllers;

use App\Traits\ConsumeExternalServices;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    use ConsumeExternalServices;

    public function auth(Request $request)
    {
        $queryParams = ['usr' => $request->login_user, 'pw' => $request->login_pw];
        $res = $this->makeRequest('GET', 'login.pro', $queryParams);
        $res = count($res) > 0 ? $res[0] : $res;

        return response()->json($res);
        //return redirect()->intended(route('spa'));
    }
    public function index(Request $request)
    {
        return view('login');
    }
}
