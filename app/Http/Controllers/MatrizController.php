<?php

namespace App\Http\Controllers;

use App\Exports\CommonCollectExport;
use App\Exports\CommonExport;
use App\Traits\ConsumeExternalServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MatrizController extends Controller
{
    use ConsumeExternalServices;

    public function filters()
    {
        $res = $this->makeRequest('GET', 'ATENCIONES_FILTROS');
        $tipos_examen = [];
        if (count($res) > 0) {
            if ($res[0]->status == 200) {
                $tipos_examen = $res[0]->tipos_examen;
            }
        }

        return compact('tipos_examen');
    }
    public function export(Request $request)
    {
        $res = $this->getDatos($request);
        if (needle($res)->status == 200) {
            return ((new CommonExport())->resources($res,'excel.matriz')->download('matriz.xlsx'));
        } else {
            return response()->json($res, 401);
        }
    }
    public function records(Request $request)
    {
        $res = $this->getDatos($request);

        return response()->json($res, needle($res)->status ?? 500);
    }

    private function getDatos($request)
    {
        $d_filters = (perdiodohelper($request->toArray()))->getPeriod();
        $queryParams = [
            'PARM_FECHA_INI' => $d_filters->start,
            'PARM_FECHA_FIN' => $d_filters->end,
            'PARM_IDUSER' => $request->user_id,
            'PARM_TOKEN' => $request->token,
            'PARM_PACIENTE' => $request->nombre,
            'PARM_TIPO_EXAMEN_ID' => $request->tipo_examen_id,
        ];

        $res = $this->makeRequest('GET', 'MATRIZ', $queryParams);

        $res = $res ? (count($res) > 0 ? $res[0] : $res) : $res;

        return $res;
    }
}
