<?php

namespace App\Traits;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

trait ConsumeExternalServices
{
    public function makeRequest($method, $requestUrl, $queryParams = [], $formParams = [], $headers = [], $isJsonRequest = true)
    {
        // Establece la Base Uri ha consumir
        $client = new Client([
            'base_uri' => env('BASE_URI_API'),
        ]);

        if (env('BASE_ENTITY')) {
            $requestUrl =   env('BASE_ENTITY') . "/$requestUrl";
        }
        // Envia en la cabezara la autorizacion
        if (method_exists($this, 'resolveAuthorization')) {
            $this->resolveAuthorization($queryParams, $formParams, $headers);
        }

        // Una vez vinculado el servicio, envio el tipo de peticion y la url del api, para ejecutar (consumir) el servicio
        $response = $client->request($method, $requestUrl, [
            $isJsonRequest ? 'json' : 'form_params' => $formParams,
            'headers' => $headers,
            'query' => $queryParams,
        ]);


        // Obtengo la respuesta del servicio consumido
        $response = $response->getBody()->getContents();

        // Decodifico la respuesta
        if (method_exists($this, 'decodeResponse')) {
            $response = $this->decodeResponse($response);
        }

        return $response;
    }
    public function makeRequestStream($method, $requestUrl, $queryParams = [], $formParams = [], $headers = [], $isJsonRequest = false)
    {
        // Establece la Base Uri ha consumir
        $client = new Client([
            'base_uri' => env('MIX_URI_RUST'),
        ]);

        // Envia en la cabezara la autorizacion
        if (method_exists($this, 'resolveAuthorization')) {
            $this->resolveAuthorization($queryParams, $formParams, $headers);
        }

        // Una vez vinculado el servicio, envio el tipo de peticion y la url del api, para ejecutar (consumir) el servicio
        $response = $client->request($method, $requestUrl, [
            $isJsonRequest ? 'json' : 'form_params' => $formParams,
            'headers' => $headers,
            'query' => $queryParams,
        ]);


        // Obtengo la respuesta del servicio consumido
        $response = $response->getBody()->getContents();

        return $response;
    }
    public function decodeResponse($value)
    {
        return json_decode($value);
    }
}
