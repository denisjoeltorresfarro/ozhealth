"use strict";(self.webpackChunk=self.webpackChunk||[]).push([[950],{8982:(t,e,o)=>{o.d(e,{Z:()=>n});var i=o(3645),a=o.n(i)()((function(t){return t[1]}));a.push([t.id,".v-enter-active[data-v-2587efc4],.v-leave-active[data-v-2587efc4]{transition:opacity .3s}.v-enter[data-v-2587efc4],.v-leave-to[data-v-2587efc4]{opacity:0}",""]);const n=a},5950:(t,e,o)=>{o.r(e),o.d(e,{default:()=>u});const i={props:{},data:function(){return{valid:!1,password:"",passwordConfirm:"",btnLoading:!1,lockState:!0,btnDisable:!1,dialogState:!1,errors:[],passRules:[function(t){return!!t||"No se puede dejar este campo vacio"},function(t){return t.length>=6||"Debe de contener más de 6 caracteres"}]}},watch:{password:function(t){t.length>6&&(this.errors=[])}},methods:{checkPass:function(t){return t!=this.password?(this.btnDisable=!0,"La confirmacion no coincide"):(this.btnDisable=!1,!0)},save:function(){var t=this,e=document.querySelector('meta[name="csrf-token"]').getAttribute("content");window.axios.defaults.headers.common["X-CSRF-TOKEN"]=e,this.btnLoading=!0,axios.post("/acount/update",{pass:this.password,passConfirm:this.passwordConfirm}).then((function(e){var o=e.data;200===o.status?(Toast.fire({icon:"success",title:o.message}),location.reload(!0),t.dialogState=!1):Toast.fire({icon:"error",title:o.message})})).catch((function(e){422==e.response.status?(Toast.fire({icon:"error",title:"Los datos ingresados no son validos"}),t.errors=e.response.data.errors):Toast.fire({icon:"error",title:e.response.data.message})})).finally((function(){t.btnLoading=!1}))},openDialog:function(){this.passwordConfirm="",this.password="",this.dialogState=!0},close:function(){this.$emit("update:return",!1),this.dialogState=!1},changeIcon:function(){this.lockState=!this.lockState}}};var a=o(1900);const n=(0,a.Z)(i,(function(){var t=this,e=t.$createElement,o=t._self._c||e;return o("div",{staticClass:"text-center"},[o("v-dialog",{attrs:{width:"500"},model:{value:t.dialogState,callback:function(e){t.dialogState=e},expression:"dialogState"}},[o("v-card",[o("v-card-title",[t._v(" Cambiar contraseña ")]),t._v(" "),o("v-card-text",[o("v-form",{model:{value:t.valid,callback:function(e){t.valid=e},expression:"valid"}},[o("v-container",[o("v-row",[o("v-col",{attrs:{cols:"12"}},[o("v-text-field",{attrs:{rules:t.passRules,"error-messages":t.errors.pass,"append-outer-icon":t.lockState?"mdi-eye-off":"mdi-eye",type:t.lockState?"password":"text",label:"Nueva contraseña",required:""},on:{"click:append-outer":t.changeIcon},model:{value:t.password,callback:function(e){t.password=e},expression:"password"}})],1),t._v(" "),o("v-col",{attrs:{cols:"12"}},[o("v-text-field",{attrs:{"error-messages":t.errors.passConfirm,"append-outer-icon":t.lockState?"mdi-eye-off":"mdi-eye",rules:[t.checkPass],type:t.lockState?"password":"text",label:"Repetir contraseña",required:""},on:{"click:append-outer":t.changeIcon},model:{value:t.passwordConfirm,callback:function(e){t.passwordConfirm=e},expression:"passwordConfirm"}})],1)],1)],1)],1)],1),t._v(" "),o("v-card-actions",{staticClass:"d-flex justify-space-between"},[o("v-btn",{staticClass:"m-2",attrs:{outlined:"",color:"red"},on:{click:t.close}},[t._v("\n          Cancelar\n        ")]),t._v(" "),o("v-btn",{staticClass:"m-2",attrs:{color:"primary",disabled:t.btnDisable,loading:t.btnLoading},on:{click:t.save}},[t._v("\n          Guardar cambios\n        ")])],1)],1)],1)],1)}),[],!1,null,null,null).exports;const r={props:["drawer","miniVariant","clipped","noAction"],data:function(){return{grupos:[]}},computed:{drawerLeft:{get:function(){return this.drawer},set:function(t){this.$emit("update:drawer",t)}}},created:function(){this.grupos=[{icon:"mdi-ambulance",clave:"KE",title:"Atenciones",items:[{icon:"mdi-file-document-outline",title:"Atenciones",to:{name:"atencion.index"}}]}]}};const s={components:{navegationDrawerLeft:(0,a.Z)(r,(function(){var t=this,e=t.$createElement,o=t._self._c||e;return o("v-navigation-drawer",{attrs:{"mini-variant":t.miniVariant,clipped:t.clipped,fixed:"",app:""},model:{value:t.drawerLeft,callback:function(e){t.drawerLeft=e},expression:"drawerLeft"}},[o("v-list",t._l(t.grupos,(function(e,i){return o("v-list-group",{key:i,attrs:{value:!1,"prepend-icon":e.icon,"no-action":t.noAction},scopedSlots:t._u([{key:"activator",fn:function(){return[o("v-list-item-content",[o("v-list-item-title",[o("div",{staticClass:"text-wrap",staticStyle:{width:"6rem"}},[t._v("\n              "+t._s(e.title)+"\n            ")])])],1)]},proxy:!0}],null,!0)},[t._v(" "),t._l(e.items,(function(e,i){return o("v-list-item",{key:i,staticClass:"ml-3",attrs:{to:e.to,router:"",exact:""}},[o("v-list-item-action",[o("v-icon",[t._v(t._s(e.icon))])],1),t._v(" "),o("v-list-item-content",[o("v-list-item-title",[o("div",{staticClass:"text-wrap",staticStyle:{width:"6rem"}},[t._v("\n              "+t._s(e.title)+"\n            ")])])],1)],1)}))],2)})),1)],1)}),[],!1,null,null,null).exports,passwordForm:n},props:["user"],data:function(){return{darkTheme:!1,clipped:!1,returnPassForm:!1,showReports:!1,showPassForm:!1,VueEnv:"development",LaravelEnv:"local",drawer:!1,fixed:!1,noAction:!1,miniVariant:!1,right:!0,rightDrawer:!1,title:"JOEL TORRES FARRO",idUser:null,ReportGroups:[{icon:"mdi-file-pdf",clave:"ARCHIVO CLINICO",title:"Archivo clínico",color:"",items:[{icon:"mdi-file-pdf",title:"Citas Solicitadas",to:{name:"report-hc-solicitudes"},clave:"ID_HCSOLICPORSERV",color:""}]},{icon:"mdi-file-pdf",clave:"LABORATORIO",title:"Laboratorio",color:"",items:[{icon:"mdi-file-pdf",title:"Resultados por Servicios",to:{name:"laboratorio.reportes.resultados.servicio"},clave:"ID_LabTipoAnalisisResultados",color:""},{icon:"mdi-file-pdf",title:"Producion por Área",to:{name:"laboratorio.reportes.produccion.area"},clave:"ID_LabProduccion",color:""},{icon:"mdi-file-pdf",title:"Producion por Servicio",to:{name:"laboratorio.reportes.produccion.servicio"},clave:"ID_LabProduccion",color:""},{icon:"mdi-file-pdf",title:"Examenes por Paciente",to:{name:"laboratorio.reportes.examen.paciente"},clave:"ID_LabProduccion",color:""},{icon:"mdi-file-pdf",title:"Consolidado por Paciente",to:{name:"laboratorio.reportes.consolidado.mensual.paciente"},clave:"ID_LabProduccion",color:""}]},{icon:"mdi-file-pdf",clave:"FARMACIA",title:"Farmacia",color:"",items:[{icon:"mdi-file-pdf",title:"ICI Diario",to:{name:"farmacia.reporte.ici_diario"},clave:"ID_FormatoICI",color:""},{icon:"mdi-file-pdf",title:"ICI/IDI Mensual",to:{name:"farmacia.reporte.ici_mensual"},clave:"ID_FormatoICI",color:""},{icon:"mdi-file-pdf",title:"Saldos por almacén",to:{name:"farmacia.reporte.saldo_almacen"},clave:"id_saldos",color:""},{icon:"mdi-file-pdf",title:"Consumo por pac/diag",to:{name:"farmacia.reporte.consumo_diagnostico"},clave:"ID_ConsumoporNCuenta",color:""},{icon:"mdi-file-pdf",title:"Consumo por cuenta",to:{name:"farmacia.reporte.consumo_cuenta"},clave:"ID_ConsumoporNCuenta",color:""},{icon:"mdi-file-pdf",title:"Consumo de farmacias",to:{name:"farmacia.reporte.consumo_total_mensualizado"},clave:"id_saldos",color:""}]},{icon:"mdi-file-pdf",clave:"HOSPITALIZACION",title:"Hospitalización",color:"",items:[{icon:"mdi-file-pdf",title:"Relación de camas",to:{name:"hospitalizacion.reporte.relacion_camas"},clave:"ID_HCamasH",color:""},{icon:"mdi-file-pdf",title:"Cuentas sin Cod. Prestación",to:{name:"hospitalizacion.reporte.cuentas_sin_codigo_prestacion"},clave:"ID_IngresosHospitalarios",color:""},{icon:"mdi-file-pdf",title:"Ingresos diarios",to:{name:"hospitalizacion.reporte.emergencia_hospitalizacion_diaria"},clave:"ID_IngresosHospitalarios",color:""}]}]}},mounted:function(){var t=this.$store.getters.GET_USER;this.title=t.name,this.idUser=t.id,this.VueEnv=this.$store.getters.GET_ENV_VUE,this.LaravelEnv=this.$store.getters.GET_ENV_LARAVEL},computed:{themeMessage:function(){return!0===this.darkTheme?"MODO DIA":"MODO NOCHE"}},methods:{changeTheme:function(){this.$vuetify.theme.dark=!this.darkTheme,this.darkTheme=!this.darkTheme},update:function(){var t=this;axios.get("/cache/clear").then((function(t){})).catch((function(t){})).finally((function(){t.$router.replace({query:null}),window.location.href=window.location.href}))},logout:function(){var t=document.querySelector('meta[name="csrf-token"]').getAttribute("content");window.axios.defaults.headers.common["X-CSRF-TOKEN"]=t,this.$store.dispatch("logout")}}};var c=o(3379),l=o.n(c),d=o(8982),p={insert:"head",singleton:!1};l()(d.Z,p);d.Z.locals;const u=(0,a.Z)(s,(function(){var t=this,e=t.$createElement,o=t._self._c||e;return o("v-app",{attrs:{dark:""}},[o("navegation-drawer-left",{attrs:{drawer:t.drawer,clipped:t.clipped,miniVariant:t.miniVariant,noAction:t.noAction},on:{"update:drawer":function(e){t.drawer=e},"update:clipped":function(e){t.clipped=e},"update:miniVariant":function(e){t.miniVariant=e},"update:mini-variant":function(e){t.miniVariant=e},"update:noAction":function(e){t.noAction=e},"update:no-action":function(e){t.noAction=e}}}),t._v(" "),o("v-app-bar",{attrs:{"clipped-left":t.clipped,fixed:"",app:""}},[o("v-app-bar-nav-icon",{on:{click:function(e){e.stopPropagation(),t.drawer=!t.drawer}}}),t._v(" "),o("v-btn",{attrs:{icon:""},on:{click:function(e){return t.$router.push({name:"home.logs"})}}},[o("v-icon",[t._v("mdi-home")])],1),t._v(" "),o("v-toolbar-title",[o("v-menu",{attrs:{bottom:"","offset-y":""},scopedSlots:t._u([{key:"activator",fn:function(e){var i=e.on,a=e.attrs;return[o("v-btn",t._g(t._b({staticClass:"ma-2",attrs:{text:""}},"v-btn",a,!1),i),[o("v-icon",{attrs:{left:"",dark:""}},[t._v(t._s(4076==t.idUser?"mdi-heart-circle-outline":"mdi-account-circle-outline")+"\n            ")]),t._v("\n            "+t._s(t.title)+"\n            "),o("v-icon",{attrs:{right:"",dark:""}},[t._v(" mdi-arrow-down-drop-circle-outline ")])],1)]}}])},[t._v(" "),o("v-list",[o("v-list-item",{on:{click:function(e){return t.$refs.passformchangeref.openDialog()}}},[o("v-list-item-action",[o("v-icon",{attrs:{light:""}},[t._v("mdi-shield-lock-outline")])],1),t._v(" "),o("v-list-item-title",[t._v("Cambiar clave web")])],1),t._v(" "),o("v-list-item",{on:{click:t.logout}},[o("v-list-item-action",[o("v-icon",{attrs:{light:""}},[t._v("mdi-power-standby")])],1),t._v(" "),o("v-list-item-title",[t._v("Cerrar sesión")])],1)],1)],1)],1),t._v(" "),o("v-spacer"),t._v(" "),"development"===t.VueEnv?o("v-chip",{staticClass:"ma-2",attrs:{color:"green accent-4",outlined:""}},[o("v-icon",{attrs:{left:""}},[t._v(" mdi-wrench")]),t._v("\n      Desarrollador VUE\n    ")],1):t._e(),t._v(" "),"local"===t.LaravelEnv?o("v-chip",{staticClass:"ma-2",attrs:{color:"red darken-4",outlined:""}},[o("v-icon",{attrs:{left:""}},[t._v(" mdi-wrench")]),t._v("\n      Desarrollador LARAVEL\n    ")],1):t._e(),t._v(" "),o("v-btn",{attrs:{fab:"",dark:"",outlined:"",title:"Actualizar",small:"",color:"primary"},on:{click:t.update}},[o("v-icon",[t._v("mdi-refresh")])],1)],1),t._v(" "),o("v-main",[o("div",{staticClass:"mx-2"},[o("transition",{attrs:{mode:"out-in"}},[o("router-view")],1)],1)]),t._v(" "),o("password-form",{ref:"passformchangeref"})],1)}),[],!1,null,"2587efc4",null).exports}}]);