<?php

use App\Http\Controllers\AtencionController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\MatrizController;
use App\Http\Controllers\SpaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::POST('/auth', [AuthController::class, 'auth'])->name('auth');
Route::POST('/auth', [AuthController::class, 'auth'])->name('login');

Route::get('/legajo', [FileController::class, 'legajo']);
Route::get('/pdf', [FileController::class, 'download']);
Route::get('/pdf-merge', [FileController::class, 'downloadMerge']);


Route::group(['prefix' => 'atencion'], function ($router) {
    Route::get('/filter', [AtencionController::class, 'filters']);
    Route::get('/records', [AtencionController::class, 'records']);
});

Route::group(['prefix' => 'matriz'], function ($router) {
    Route::get('/filter', [MatrizController::class, 'filters']);
    Route::get('/records', [MatrizController::class, 'records']);
    Route::get('/download', [MatrizController::class, 'export']);
});

//Route::get('/login', [AuthController::class, 'index'])->name('login.index');
Route::get('/spa', [SpaController::class, 'index'])->name('spa.index');
Route::get('/{any?}', [SpaController::class, 'index'])->name('spa')->where('any', '.*');
